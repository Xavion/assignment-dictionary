global exit
global string_length
global write_string
global print_string
global err_string
global write_char
global print_char
global print_newline
global print_int
global print_uint
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_equals
global string_copy

section .text

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
  .end:
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, 1
    push rdi
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor rcx, rcx
    mov rax, rdi
    mov rdi, 10
    push 0x00
.loop:
    xor rdx, rdx
    div rdi
    add rdx, '0'
    push rdx
    test rax, rax
    jnz .loop
.out:
    pop rdi
    cmp rdi, 0x00
    je .exit
    call print_char
    jmp .out
.exit:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .unsigned
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.unsigned:
    call print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor rdx, rdx
.loop:
    mov al, byte[rdi+r8]
    mov dl, byte[rsi+r8]
    cmp al, dl
    jne .false
    inc r8
    cmp al, 0x00
    je .true
    jmp .loop
.false:
    xor rax, rax
    ret
.true:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret


%macro read_template 2
%1:
    xor rcx, rcx
.loop:
    cmp rcx, rsi
    jg .fail
    
    push rcx
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    pop rcx

%if %2 == 1
    cmp al, 0x20
    je .success
    cmp al, 0x9
    je .success
%endif

    cmp al, 0xA
    je .success
    cmp al, 0       ; end of file (Ctrl + D)
    jle .end

    mov byte[rdi + rcx], al
    inc rcx
    jmp .loop

.fail:
    mov rax, 0
    ret

.success:
    test rcx, rcx
    jz .loop
.end:
    mov byte[rdi + rcx], 0x0
    mov rax, rdi
    mov rdx, rcx
    ret
%endmacro

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

section .data
buffer times 256 db 0
section .text


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov rsi, rdi
	xor rdx, rdx
	xor rax, rax
.loop:
	xor rdi, rdi
	cmp byte[rsi+rdx], '9'
	ja .exit
	cmp byte [rsi+rdx], '0' 
	jb .exit

	mov dil, byte [rsi+rdx]

	sub dil, '0'
	imul rax, 10
	add rax, rdi

	inc rdx
	jmp .loop 
.exit:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'
	jne parse_uint
	inc rdi
	call parse_uint
	inc rdx
	neg rax
	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
.loop:
	xor rcx, rcx
	mov cl, byte[rdi]
	mov byte[rsi], cl
	cmp rsi, rdx                           
	jz .exit
	inc rdi
	inc rsi
	test rcx, rcx
	jnz .loop
.exit:
	xor rax, rax
	ret
;Вывод строки, где в rsi передается дескриптор для вывода. 1 - stdout, 2 - stderr
write_string:
    push rsi
    call string_length
    mov rsi, rdi
    mov rdx, rax

    mov rax, 1
    pop rdi
    syscall
    ret

print_string:
    mov rsi, 1
    jmp write_string

read_template read_word, 1
read_template read_line, 0
err_string:
    mov rsi, 2
    jmp write_string

