%define NODE_SIZE 8

global find_word
extern string_equals

section .text

find_word:
.loop:
    test rsi, rsi
    jz .end

    add rsi, NODE_SIZE
    call string_equals
    test rax, rax
    jnz .end
    
    mov rsi, [rsi - NODE_SIZE]
    jmp .loop

.end:
    mov rax, rsi
    ret
