%include "words.inc"

%define BUFFER_SIZE 255

global _start
extern read_line
extern find_word
extern string_length
extern print_string
extern print_newline
extern exit
extern err_string

section .data
err_msg: db 'Cannot find element in list', 10, 0 ;

section .text
_start:
    sub rsp, BUFFER_SIZE
    mov rdi, rsp
    mov rsi, BUFFER_SIZE
    call read_line

    mov rsi, last
    mov rdi, rax
    call find_word

    add rsp, BUFFER_SIZE
    
    test rax, rax
    jz .fail

    mov rdi, rax
    call string_length

    add rdi, rax
    inc rdi
    call print_string
    call print_newline

    mov rdi, 0
    call exit

.fail:
    mov rdi, err_msg
    call err_string
    
    mov rdi, 1
    call exit
